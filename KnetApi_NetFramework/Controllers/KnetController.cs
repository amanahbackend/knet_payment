﻿using KnetApi_NetFramework.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace KnetApi_NetFramework.Controllers
{
    [Route("api/Knet")]
    public class KnetController : ApiController
    {
        protected KnetController()
        {
        }

        [HttpPost, Route("api/Knet/Pay")]
        public IHttpActionResult Pay([FromBody] KnetPaymentViewModel model)
        {
            try
            {
                var resourcePath = ConfigurationManager.AppSettings["resourceFilePath"].ToString();
                var MyObj = new e24PaymentPipeLib.e24PaymentPipeCtlClass
                {
                    Action = "1",            // Purchase Transaction
                    Amt = model.Amount.ToString(),          // The amount of purchase
                    Currency = "414",          // KD Currency
                    Language = "USA",         // Payment Page Language
                    ResponseUrl = model.SuccessUrl, // Your response URL where you will be notified with of transaction response
                    ErrorUrl = model.ErrorUrl, //
                    TrackId = (new Random().Next(10000000) + 1).ToString(), // You should create a new unique track ID for each transaction
                    ResourcePath = resourcePath, // Directory to your resource.cgn ending with \
                    Alias = model.Alias, // Alias of the plug-in
                    Udf1 = model.CartId.ToString(),
                    Udf2 = "", //Session["UserID"].ToString();
                    Udf3 = model.Source,
                    Udf4 = "",
                    Udf5 = ""
                };

                var TransVal = MyObj.PerformInitTransaction();  //return 0 for success otherwise -1 for failure
                var rawResponse = MyObj.RawResponse;
                var paymentID = MyObj.PaymentId;
                var paymentPage = MyObj.PaymentPage;
                var errorMsg = MyObj.ErrorMsg;
                string url = $"{paymentPage}?PaymentID={paymentID}";
                return Ok(url);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }

        }

        [HttpGet, Route("api/Knet/Response")]
        public async Task<HttpResponseMessage> Response()
        {
            var query = Request.GetQueryNameValuePairs();            
            var url = ConfigurationManager.AppSettings["mawashiSuccessService"].ToString();
            HttpClient client = new HttpClient();

            var content = new FormUrlEncodedContent(query);

            var response = await client.PostAsync(url, content);

            //var responseString = await response.Content.ReadAsStringAsync();
            string result = "REDIRECT=http://store.almawashi.com.kw";
            var resp = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(result, Encoding.UTF8, "text/plain")
            };
            return resp;
        }

        [HttpGet, Route("api/Knet/Error")]
        public async Task<HttpResponseMessage> Error()
        {
            var query = Request.GetQueryNameValuePairs();
            var url = ConfigurationManager.AppSettings["mawashiErrorService"].ToString();
            HttpClient client = new HttpClient();

            var content = new FormUrlEncodedContent(query);

            var response = await client.PostAsync(url, content);

            string result = "REDIRECT=http://store.almawashi.com.kw";
            var resp = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(result, Encoding.UTF8, "text/plain")
            };
            return resp;
        }
    }
}
