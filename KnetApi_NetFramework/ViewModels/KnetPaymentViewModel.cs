﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnetApi_NetFramework.ViewModels
{
    public class KnetPaymentViewModel
    {
        public double Amount { get; set; }
        public int CartId { get; set; }
        public string ErrorUrl { get; set; }
        public string SuccessUrl { get; set; }
        public string Source { get; set; }
        public string Alias { get; set; }
    }
}